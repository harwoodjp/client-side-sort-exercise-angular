var simpleApp = angular.module('simpleApp', []);

simpleApp.controller('simpleCtrl', function($scope) {

    $scope.first_name = "first";
    $scope.last_name = "last";

    $scope.change_first = function() {
        $scope.first_name = $scope.text_input;
    };

    $scope.change_last = function() {
        $scope.last_name = $scope.text_input2;
    };
    }
);

simpleApp.controller('peopleCtrl', function($scope, $http) {
    $http.get('client_sort/people.json').success(function(data) {
            $scope.sort_type = '';
            $scope.names = data;
            console.log("client_sort/people.json" , $scope.names);
        });


    $scope.sort1 = function() {
        $scope.sort_type = "first_name";
    };
    $scope.sort2 = function() {
        $scope.sort_type = "last_name";
    };
    $scope.sort3 = function() {
        $scope.sort_type = "-first_name";
    };
    $scope.sort4 = function() {
        $scope.sort_type = "-last_name";
    };
    $scope.sort5 = function() {
        $scope.sort_type = "age";
    };
    $scope.sort6 = function() {
        $scope.sort_type = "-age";
    };

});
